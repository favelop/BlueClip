# BlueClip

<img src="android/ic_launcher-web.png" alt="logo" width=120px />

Synchronize your clipboard via Bluetooth between Android and Desktop (Linux and probably Windows/Mac).

**This was created in 2014 and works on Android up to version 8 (later not tested).**

The Android app registers itself as a service and only runs, wenn Bluetooth is active and someone connects.
To use the CLI on your Desktop you need to have Java SE 1.6 or higher installed on your system.
BlueClip uses the [BlueCove](http://bluecove.org/) library .
At least on Linux you also need the BlueZ libraries.

Debian:

    sudo apt install libbluetooth-dev

## Usage

    java -jar blueclip-cli-experimental.jar -l
    
or
    
    java -jar blueclip-cli-experimental.jar -m MAC_ADDRESS