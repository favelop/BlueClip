package com.fas.blueclip;
import java.util.List;
import java.util.Scanner;


public class CliManager {
	public static void out(Object out) {
		System.out.println(out);
	}
	
	public static void pout(Object out) {
		System.out.print(out);
	}
	
	public static String in() {
		return new Scanner(System.in).next().toLowerCase();
	}
	
	public static void help() {
		out("Usage: java -jar blueclip-cli.jar [OPTIONS]");
		out("");
		out("Options:");
		out("  -h, --help                    print this help and exit");
		out("  -k                            unused");
		out("  -l                            select manually from a list of available devices");
		out("  -m MAC_ADDRESS                connect to specified mac address");
		out("  -r                            restart if connection failed");
		out("  -t TIMEOUT_MS                 timeout before restarting in ms (default=5000)");
		out("");
		out("Without providing options blueclip-cli will search for visible devices and connect to first available service");
	}
	
	public static void serviceFound(String url) {
		out("Service found: \"" + url + "\" ");
	}
	
	public static void deviceFound(String address, String name) {
		Device d = new Device(address, name);
		out("Device found: " + d.toString());
	}
	
	public static void noServicesFound() {
		out("No services found");
	}
	
	public static void sentClipboard(String clipboard, boolean sent) {
		out("---");
		out(clipboard);
		out("---");
		out(sent ? "Succesfully sent " : "Failed to send " + "clipboard content");
	}
	
	public static void start() {
		out("BlueClip started");
	}
	
	public static void close() {
		out("BlueClip shut down");
	}
	
	public static void connect(String device) {
		out("Connecting to \"" + device + "\"...");
	}
	
	public static void connectionError(String error) {
		out("Could not connect: " + error);
	}
	
	public static void setClipboard(String clipboard) {
		out("---");
		out(clipboard);
		out("---");
		out("Received clipboard and set it");
	}
	
	public static void displayList(List<? extends Object> l) {
		out("---");
		for (int i = 0; i < l.size(); i++) {
			out((i + 1) + " - " + l.get(i).toString());
		}
		out("---");
	}
	
	public static int displaySelectList(List<? extends Object> l) {
		int selection = -1;
		displayList(l);
		pout("Please select an item: ");
		while (selection == -1) {
			try {
				selection = Integer.parseInt(in());
				if (selection < 1 && selection > l.size()) {
					selection = -1;
				}
			} catch (NumberFormatException e) {
				out(e.getMessage());
			}
		}
		return selection;
	}
	
}
