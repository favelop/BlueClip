package com.fas.blueclip;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.LinkedList;
import java.lang.IllegalArgumentException;
public class ArgParser{
	
	private Map<String, List<String>> flags = new HashMap<String, List<String>>();	
	
	public ArgParser(String[] args){
		String key = "default";
		List<String> l = new LinkedList<String>();
		for(String s : args){
			if (s.charAt(0)== '-') {
				key = s;
				l = new LinkedList<String>();
			} else 	l.add(s);
			flags.put(key, l);
		}
	}

	/**
	 * checks if arguments contains argument
	 * @param  str argument
	 * @return     true if argument is contained
	 */
	public boolean has(String str){
		return flags.containsKey(str);
	}

	/**
	 * gets corresponding value for an argument
	 * @param  str argument to get value from
	 * @return     corresponding value
	 */
	public String get(String str) {
		if(has(str) && flags.get(str).size() != 0){
			return flags.get(str).get(0);
		}else {
			throw new IllegalArgumentException("Argument after flag \""+str+"\" invalid or not apparent");
		}
	}

}