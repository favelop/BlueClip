package com.fas.blueclip;
public class Device {
	private String name;
	private String address;
	private String url;

	/**
	 * Device constructor
	 */
	public Device(String address, String name) {
		this.address = address;
		this.name = name;
	}
	
	public Device(String address) {
		this(address, address);
	}

	/**
	 * gets the devices name
	 * @return the name of the device
	 */
	public String getName() {
		return name;
	}

	/**
	 * gets the devices address
	 * @return the address of the device
	 */
	public String getAddress() {
		return address;
	}
	
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	
	@Override
	public String toString() {
		return name + " (" + address + ")";
	}
}