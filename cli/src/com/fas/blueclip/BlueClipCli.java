package com.fas.blueclip;
/**
 * @author Fabian, Nils
 */
public class BlueClipCli {
	public static void main(String[] args) {
		ArgParser parser = new ArgParser(args);
		
		if (parser.has("-h") || parser.has("--help")) {
			CliManager.help();
			return;
		}
		
		ClipSynchronizer sync = new ClipSynchronizer();
		
		if (parser.has("-k")) {
			// is known devices really possible??
		}
		
		if (parser.has("-l")) {
			sync.setDisplayList();
		}
		
		if (parser.has("-m")) {
			sync.setDeviceMac(parser.get("-m"));
		}
		
		if (parser.has("-r")) {
			sync.setAutoRestart();
		}
		
		if (parser.has("-t")) {
			try {
				sync.setRestartTimeout(Integer.parseInt(parser.get("-t")));
			} catch (NumberFormatException e) {
				CliManager.out(e.getMessage());
			}
		}
		
		new Thread(sync).start();
	}

}