package com.fas.blueclip;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.datatransfer.StringSelection;
import java.io.IOException;
import java.util.List;

public class ClipSynchronizer implements ClipboardOwner, Runnable{
    private Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
    private ConnectionProvider provider;
    
    private String deviceMac = null;
    private boolean displayList = false;
    private boolean autoRestart = false;
    private int restartTimeout = 5000;
    
    private boolean running;

    /**
     * ClipSynchronizer constructor 
     */
    public ClipSynchronizer(){
        provider = new BluetoothProvider();
        regainOwnership();
    }
    
    public void setAutoRestart() {
    	autoRestart = true;
    }
    
    public void setDisplayList() {
    	displayList = true;
    }
    
    public void setDeviceMac(String address) {
    	deviceMac = address.replace(":", "").toUpperCase();
    }
    
    public void setRestartTimeout(int timeout) {
    	restartTimeout = timeout;
    }

    @Override
    public void lostOwnership(Clipboard clip, Transferable tran) {
        if(!running)return;
        try{
            Thread.sleep(20L);  // necessary to ensure correct behaviour on windows systems
        } catch (Exception e){
        	CliManager.out(e.getMessage());
        }
        tran = clip.getContents(null);
        if (tran != null && tran.isDataFlavorSupported(DataFlavor.stringFlavor)) {
            try {
            	String clipboardContent = (String)tran.getTransferData(DataFlavor.stringFlavor);
                CliManager.sentClipboard(clipboardContent, provider.sendMessage(clipboardContent));
            } catch (UnsupportedFlavorException ex) {
            	CliManager.out(ex.getMessage());
            } catch (IOException ex) {
            }
        }
        regainOwnership();
    }

    /**
     * gain ownership of current clipboard
     */
    private void regainOwnership() {
        clip.setContents(clip.getContents(null), this);
    }
    
    public void stop() {
    	running = false;
        System.exit(0);
    }

    @Override
    public void run() {
    	List<Device> devices;
    	
    	CliManager.start();
    	
    	if (deviceMac == null) {
    		devices = provider.getDevices();
    		
    		if (devices.size() > 0) {
	    		if (displayList) {
	    			deviceMac = devices.get(CliManager.displaySelectList(devices) - 1).getAddress();
	    		} else {
    				deviceMac = devices.get(0).getAddress();
	    		}
			} else {
				CliManager.noServicesFound();
			}
    	}
    	
    	if (deviceMac != null) {
	    	do {
		    	running = true;
		    	if (provider.connect(deviceMac)) {
			        try {
			            while (running) {
			                String message = provider.getMessage();
			                clip.setContents(new StringSelection(message), this);
			                CliManager.setClipboard(message);
			            }
			        } catch (Exception e) {}
			        provider.close();
		    	} else {
		    		try {
						Thread.sleep(restartTimeout);
					} catch (InterruptedException e) {}
		    	}
	    	} while (autoRestart);
    	}
    	
    	CliManager.close();
        stop();
    }
}
