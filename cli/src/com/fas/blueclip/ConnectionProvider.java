package com.fas.blueclip;
import java.io.IOException;
import java.util.List;
public interface ConnectionProvider{
	/**
	 * opens a new connection
	 * @return true if connection was successfully established, else false
	 */
	public boolean connect(String device);
	/**
	 * closes the  current connection
	 */
	public void close();
	/**
	 * sends a message over the current connection
	 * @return true if message was successfully sent, else false
	 */
	public boolean sendMessage(String message);

	/**
	 * receives next message and blocks when there is nothing to receive
	 * @return received message
	 * @throws IOException 
	 */
	public String getMessage() throws IOException;

	/**
	 * searches available devices  
	 * @return a list of available devices
	 */
	public List<Device> getDevices();

	/**
	 * retrieves a list of known devices
	 * @return a list of known devices
	 */
	public List<Device> getKnownDevices();
}