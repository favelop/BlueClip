package com.fas.blueclip;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.bluetooth.BluetoothStateException;
import javax.bluetooth.DeviceClass;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.DiscoveryListener;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.ServiceRecord;
import javax.bluetooth.UUID;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;

public class BluetoothProvider implements ConnectionProvider, DiscoveryListener {
	
	public static final UUID UUID = new UUID("d87442e00d0e4a3ab4bf436fd57ec2ac", false);
	
	private Map<String, List<String>> cache;
	private List<RemoteDevice> devices;
	private List<Device> devicesWithService;
	private RemoteDevice current;
	
	private StreamConnection connection;
	private DataInputStream in;
	private DataOutputStream out;
	
	/**
	 * BluetoothProvider constructor 
	 */
	public BluetoothProvider(){
		cache = new HashMap<String, List<String>>();
	}
	
	private class KnownRemoteDevice extends RemoteDevice {
		public KnownRemoteDevice(String addr) {
			super(addr);
		}
	}


	@Override
	public boolean connect(String device){
		CliManager.connect(device);
		
		try {
			if (!cache.containsKey(device)) {
				searchServicesOnDevice(new KnownRemoteDevice(device));
			}
			
			connection = (StreamConnection) Connector.open(cache.get(device).get(0));
			
			in = connection.openDataInputStream();
			out = connection.openDataOutputStream();
			
		} catch (Exception e) {
			CliManager.connectionError(e.getMessage());
			return false;
		}
		
		return true;
	}

	@Override
	public void close(){
		try {
			in.close();
		} catch (IOException e) {}
		try {
			out.close();
		} catch (IOException e) {}
		try {
			connection.close();
		} catch (IOException e) {}
		return;
	}

	@Override
	public boolean sendMessage(String message){
		try {
			out.writeUTF(message);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public String getMessage() throws IOException{
		return in.readUTF();
	}

	@Override
	public List<Device> getDevices(){
		try {
			LocalDevice local = LocalDevice.getLocalDevice();
	        DiscoveryAgent agent = local.getDiscoveryAgent();
	        
	        devices = new Vector<RemoteDevice>();
	        devicesWithService = new Vector<Device>();
	        
	        synchronized (this) {
	            agent.startInquiry(DiscoveryAgent.LIAC, this);
	        	try {
					wait();
				} catch (InterruptedException e) {
					CliManager.out(e.getMessage());
					return new Vector<Device>();
				}
	        }
	        
        	for (RemoteDevice r : devices) {
        		searchServicesOnDevice(r);
        	}
	        
	        return devicesWithService;
		} catch (Exception e) {
			CliManager.out(e.getMessage());
			return new Vector<Device>();
		}
	}
	
	/**
	 * Searches for services on RemoteDevice
	 * @param device RemoteDevice to search services on
	 * @throws BluetoothStateException
	 */
	public void searchServicesOnDevice(RemoteDevice device) throws BluetoothStateException {
		LocalDevice local = LocalDevice.getLocalDevice();
        DiscoveryAgent agent = local.getDiscoveryAgent();
        
		current = device;
		synchronized (this) {
			agent.searchServices(null, new UUID[]{UUID}, current, this);
			try {
				wait();
			} catch (InterruptedException e) {
				//return false;
			}
		}
	}

	@Override
	public List<Device> getKnownDevices(){
		return null;
	}

	@Override
	public void deviceDiscovered(RemoteDevice arg0, DeviceClass arg1) {
		devices.add(arg0);
		try {
			CliManager.deviceFound(arg0.getBluetoothAddress(), arg0.getFriendlyName(false));
		} catch (Exception e) {
			CliManager.out(e.getMessage());
		}
	}

	@Override
	public void inquiryCompleted(int arg0) {
		synchronized (this) {
			notifyAll();
		}
	}

	@Override
	public void serviceSearchCompleted(int arg0, int arg1) {
		synchronized (this) {
			notifyAll();
		}
	}

	@Override
	public void servicesDiscovered(int arg0, ServiceRecord[] arg1) {
		LinkedList<String> urls = new LinkedList<String>();
		for(ServiceRecord r : arg1) {
			urls.add(r.getConnectionURL(ServiceRecord.NOAUTHENTICATE_NOENCRYPT, false));
			CliManager.serviceFound(urls.getLast());
		}
		cache.put(current.getBluetoothAddress(), urls);
		
		if (devicesWithService != null) {
			try {
				devicesWithService.add(new Device(current.getBluetoothAddress(), current.getFriendlyName(false)));
			} catch (Exception e) {
				CliManager.out(e.getMessage());
			}
		}
	}
}
