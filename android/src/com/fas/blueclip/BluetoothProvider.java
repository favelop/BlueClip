package com.fas.blueclip;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;

public class BluetoothProvider {
	public static final java.util.UUID UUID = java.util.UUID.fromString("d87442e0-0d0e-4a3a-b4bf-436fd57ec2ac");
	
	private BluetoothServerSocket server;
	private BluetoothSocket socket;
	
	private DataInputStream in;
    private DataOutputStream out;
	
	public BluetoothProvider() throws IOException {
		BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
		
		server = adapter.listenUsingInsecureRfcommWithServiceRecord("BlueClip", UUID);
		socket = server.accept();
		
		in = new DataInputStream(socket.getInputStream());
		out = new DataOutputStream(socket.getOutputStream());
	}
	
	/**
	 * Gets the next message from remote and blocks
	 * until there is no message yet.
	 * @return received message
	 * @throws IOException
	 */
	public String getMessage() throws IOException {
		return in.readUTF();
	}
	
	/**
	 * Sends a new message to remote
	 * @param message message to send
	 * @return true if there was no exception
	 */
	public boolean sendMessage(String message) {
		try {
			out.writeUTF(message);
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	
	/**
	 * Closes active connection
	 */
	public void close() {
		if (socket != null) {
			try {
				socket.close();
			} catch (IOException e) {}
		}
		
		if (server != null) {
			try {
				server.close();
			} catch (IOException e) {}
		}
	}
	
	/**
	 * Gets readable name of connected device
	 * @return name of remote
	 */
	public String getRemoteName() {
		if (socket != null) {
			return socket.getRemoteDevice().getName();
		}
		
		return "";
	}
}
