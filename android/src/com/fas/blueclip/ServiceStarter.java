package com.fas.blueclip;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * BroadcastReceiver, that is triggered on boot completion to start
 * BlueClip service.
 */
public class ServiceStarter extends BroadcastReceiver {
	
	public static Intent service;

	@Override
	public void onReceive(Context context, Intent intent) {
		BluetoothAdapter adapter;
		

		if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)
				|| intent.getAction().equals(BlueClipService.MANUAL_START)) {
			adapter = BluetoothAdapter.getDefaultAdapter();
			if (adapter != null && adapter.isEnabled()) {
				startService(context);
			}
		}
		
		if (intent.getAction().equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
            switch (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                    BluetoothAdapter.ERROR)) {
            case BluetoothAdapter.STATE_OFF:
            	stopService(context);
                break;
            case BluetoothAdapter.STATE_ON:
            	startService(context);
                break;
            }
		}
	}
	
	/**
	 * Another abstraction layer for starting service.
	 * @param context
	 */
	private void startService(Context context) {
		if (service == null) {
			service = new Intent(context, BlueClipService.class);
	    	context.startService(service);
		}
	}
	
	/**
	 * Another abstraction layer for stopping running service.
	 * @param context
	 */
	private void stopService(Context context) {
    	if (service != null) {
    		context.stopService(service);
    		service = null;
    	}
	}
}
