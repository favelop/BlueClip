package com.fas.blueclip;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceActivity;

public class BlueClipSettings extends PreferenceActivity {

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		addPreferencesFromResource(R.xml.preferences);

		sendBroadcast(new Intent(BlueClipService.MANUAL_START));
	}
}
