package com.fas.blueclip;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat.*;

/**
 * Service that handles connections as host and syncs clipboard.
 */
public class BlueClipService extends Service implements SyncListener{
	
	public static final String MANUAL_START = "com.fas.blueclip.service.MANUAL_START";
	
	private boolean prefNotifyBg = false;
	private boolean prefNotifySync = true;
	
	private ServiceHandler handler;
	private ClipSynchronizer sync;
	private boolean running;
	
	private int notifyID = 342589;
	private NotificationManager notifyManager;
	private Builder notifyBuilder;
	
	private boolean inSync;
	
	private final class ServiceHandler extends Handler {
	     public ServiceHandler(Looper looper) {
	         super(looper);
	     }
	     @Override
	     public void handleMessage(Message msg) {
	 		while (running) {
				sync.doSync();
			}
	     }
	}

	@Override
	public void onCreate() {
		HandlerThread thread = new HandlerThread("ServiceStartArguments",
				HandlerThread.MIN_PRIORITY);
		thread.start();
		
		handler = new ServiceHandler(thread.getLooper());
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		sync = new ClipSynchronizer((ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE), this);
		
		running = true;
		handler.sendMessage(handler.obtainMessage());
		
		return START_STICKY;
	}
	
	@Override
	public void onDestroy() {
		running = false;
		if (sync != null) {
			sync.finishSync();
		}
		
		notifyManager.cancel(notifyID);
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}
	
	/**
	 * Updates ongoing notification of service
	 */
	private void updateNotification() {
		updateSettings();
		if (prefNotifySync && (prefNotifyBg || inSync)) {
			notifyManager.notify(notifyID, notifyBuilder.build());
		} else {
			notifyManager.cancel(notifyID);
		}
	}
	
	/**
	 * Updates settings for service
	 */
	public void updateSettings() {
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
		prefNotifyBg = sharedPref
				.getBoolean("show_notification_bg", prefNotifyBg);
		prefNotifySync = sharedPref
				.getBoolean("show_notification_sync", prefNotifySync);
	}

	@Override
	public void onStart() {
		notifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		notifyBuilder = new Builder(this)
			.setContentIntent(PendingIntent.getActivity(
					getApplicationContext(),
					0,
					new Intent(this, BlueClipSettings.class),
					PendingIntent.FLAG_UPDATE_CURRENT));
		onLost();
	}

	@Override
	public void onSynchronization(String name) {
		notifyBuilder
			.setContentTitle(getString(R.string.app_name) + ": " + name)
			.setSubText(getString(R.string.is_synchronized))
			.setSmallIcon(R.drawable.ic_launcher);
		inSync = true;
		updateNotification();
	}

	@Override
	public void onClipboardChange(String content) {
		notifyBuilder
			.setContentText(content)
			.setStyle(new BigTextStyle().bigText(content));
		notifyManager.cancel(notifyID); //workaround to force resizing
		updateNotification();
	}

	@Override
	public void onLost() {
		notifyBuilder
			.setContentTitle(getString(R.string.app_name))
			.setSubText(getString(R.string.not_synchronized))
	    	.setSmallIcon(R.drawable.ic_inactive);
		inSync = false;
		updateNotification();
	}
}
