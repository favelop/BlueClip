package com.fas.blueclip;

public interface SyncListener {
	
	/**
	 * Triggered when awaiting clients.
	 */
	public void onStart();
	
	/**
	 * Triggered when client connects.
	 */
	public void onSynchronization(String name);
	
	/**
	 * Triggered when content of clipboard changed
	 */
	public void onClipboardChange(String content);
	
	/**
	 * Triggered when client disconnects.
	 */
	public void onLost();
	
}
