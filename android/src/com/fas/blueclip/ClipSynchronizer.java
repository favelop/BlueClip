package com.fas.blueclip;

import java.io.IOException;

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.ClipboardManager.OnPrimaryClipChangedListener;

public class ClipSynchronizer implements OnPrimaryClipChangedListener {
	
	private ClipboardManager clipboard;
	private BluetoothProvider provider;
	private SyncListener listener;
	
	public ClipSynchronizer(ClipboardManager clipboard, SyncListener listener) {
		this.clipboard = clipboard;
		this.clipboard.addPrimaryClipChangedListener(this);
		this.listener = listener;
		listener.onStart();
		onPrimaryClipChanged();
	}

	@Override
	public void onPrimaryClipChanged() {
		StringBuilder message = new StringBuilder("");
		ClipData clip = clipboard.getPrimaryClip();
		if (clip != null) {
			for (int i = 0; i < clip.getItemCount(); i++) {
				message.append(clip.getItemAt(i).getText());
			}
		}
		listener.onClipboardChange(message.toString());
		if (provider != null) {
			provider.sendMessage(message.toString());
		}
	}
	
	/**
	 * Retrieves sync updates with BluetoothProvider
	 * and accepts new connection if necessary
	 * @return return false if there was an exception during sync
	 */
	public boolean doSync() {
		CharSequence message;
		ClipData clip;
		
		try {
			if (provider == null) {
				provider = new BluetoothProvider();
				listener.onSynchronization(provider.getRemoteName());
				onPrimaryClipChanged();
			}
			
			message = provider.getMessage();
			
			clip = new ClipData(new ClipDescription("BlueClip",
					new String[] {ClipDescription.MIMETYPE_TEXT_PLAIN}),
					new ClipData.Item(message));
			
			clipboard.removePrimaryClipChangedListener(this);
			clipboard.setPrimaryClip(clip);
			clipboard.addPrimaryClipChangedListener(this);
			
			listener.onClipboardChange(message.toString());
		} catch (IOException e) {
			if (provider != null) {
				provider.close();
				provider = null;
				listener.onLost();
			}
			return false;
		}
		return true;
	}
	
	/**
	 * Closes connection and removes clipboardchange listener
	 */
	public void finishSync() {
		clipboard.removePrimaryClipChangedListener(this);
		if (provider != null) {
			provider.close();
		}
	}
}
